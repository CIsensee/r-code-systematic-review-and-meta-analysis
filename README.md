# R Code for Systematic Review and Meta Analysis

This project presents all codes related to the research paper "The relationship between organizational culture, sustainability, and digitalization in SMEs: A systematic review."